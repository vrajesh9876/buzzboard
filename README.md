# BuzzBoard

## Prequisites (Development):

| Module | Version |
| --- | --- |
| Node | 10.16.0 |
| Npm | 6.9.0 |
| Mongodb | 4.2.1 |


##### Take Clone of project
> git clone -b git_url  folder_name


##### Install node modules
> npm install

##### Start the server
> node server.js