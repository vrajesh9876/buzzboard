/*************************************************************************************
    Configuration - QA
**************************************************************************************/
module.exports = {
    dbUrl: 'mongodb://localhost/orders',
    mongoDBOptions: {
        poolSize: 5,
        keepAlive: 1,
        native_parser: true,
        useCreateIndex: true,
        useNewUrlParser: true,
        connectTimeoutMS: 30000,
        useUnifiedTopology: true,
    },

    baseApiUrl: '/api',

    // Ports
    serverPort: '5040',
};