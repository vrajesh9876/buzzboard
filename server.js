const config = require('./configs/configs');
const mongoose = require('mongoose');
const express = require('express');
const app = express();

/*      Swagger initialization  */

const orders = require('./app/modules/orders/Orders');

mongoose.connect(config.dbUrl, config.mongoDBOptions)
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'));

app.use(express.json());

app.use(`${config.baseApiUrl}/orders`, orders);

//  Sample Route
app.get('/', (req, res) => {
    return res.send('Sample CRUD operations');
});

app.listen(config.serverPort, () => console.log(`Listening on port ${config.serverPort}...`));