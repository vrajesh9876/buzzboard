const projection = require('./Projection.json');
const Validators = require('./Validator');
const { Orders } = require('./Schema');
const express = require('express');
const router = express.Router();
const _ = require('lodash');

router.post('/create', Validators.createOrderValidate, async (req, res) => {
    try {
        //  Get the request data
        const { order_id, item_name, cost, order_date, delivery_date } = req.body;

        //  Check whether the order_id is already existed or not
        const dupOrderId = await Orders.findOne({ order_id });
        if (!_.isEmpty(dupOrderId)) return res.send({ status: 0, message: 'Order id is already existed' });

        //  Data for storing
        const order = new Orders({ order_id, item_name, cost, order_date, delivery_date });

        //  Store the data
        await order.save();

        return res.send({ status: 1, message: 'Order saved successfully' });
    } catch (error) {
        console.error('error In ====>>>> create order <<<<====', error);
        return res.send({ status: 0, message: 'Internal Server Error' });
    }

});

//  Update delivery date
router.post('/update', Validators.updateDeliveryDateValidator, async (req, res) => {
    try {
        //  Get the request data
        const { order_id, delivery_date } = req.body;

        //  Update the data
        const updateDate = await Orders.updateOne({ order_id }, { delivery_date }, { new: true });
        if (_.isEmpty(updateDate)) return res.send({ status: 0, message: 'Please provide proper order id' });

        return res.send({ status: 1, message: 'Delivery date updated successfully' });
    } catch (error) {
        console.error('error In ====>>>> deliveryDate  update <<<<====', error);
        return res.send({ status: 0, message: 'Internal Server Error' });
    }
});

//  List of orders or list of orders in specific date
router.post('/list', Validators.ordersListValidator, async (req, res) => {
    try {
        //  Get the request body
        const { order_date } = req.body;

        //  List of orders
        const list = await Orders.find({ order_date }).select(projection.ordersList);
        return res.send({ status: 1, message: 'Orders list found successfully', data: list });
    } catch (error) {
        console.error('error In ====>>>> Orders list <<<<====', error);
        return res.send({ status: 0, message: 'Internal Server Error' });
    }
});

//  Search order id
router.post('/search', Validators.orderIdSearchValidator, async (req, res) => {
    try {
        //  Get the request body
        const { order_id } = req.body;

        //  List of orders
        const data = await Orders.findOne({ order_id }).select(projection.ordersDetails);
        if (_.isEmpty(data)) return res.send({ status: 0, message: 'Orders details not found' });
        return res.send({ status: 1, message: 'Orders details found successfully', data });
    } catch (error) {
        console.error('error In ====>>>> Search Order <<<<====', error);
        return res.send({ status: 0, message: 'Internal Server Error' });
    }
});

//  Deleting an order
router.delete('/delete', Validators.orderIdSearchValidator, async (req, res) => {
    try {
        //  Get the request body
        const { order_id } = req.body;

        //  Delete order
        const removeOrder = await Orders.findOneAndRemove({ order_id });
        if (_.isEmpty(removeOrder)) return res.send({ status: 0, message: 'Please provide proper order id' });
        return res.send({ status: 1, message: 'Order Removed successfully' });
    } catch (error) {
        console.error('error In ====>>>> Delete Order <<<<====', error);
        return res.send({ status: 0, message: 'Internal Server Error' });
    }
});

module.exports = router; 