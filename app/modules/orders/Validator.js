const Joi = require('joi');

const createOrderValidate = (req, res, next) => {
    const schema = Joi.object().keys({
        order_id: Joi.string().required().label('order id'),
        item_name: Joi.string().required().label('item name'),
        cost: Joi.string().required().label('cost'),
        order_date: Joi.string().regex(/^(\d{4})(\/)(\d{1,2})(\/)(\d{1,2})$/).required().label('order date'),
        delivery_date: Joi.string().regex(/^(\d{4})(\/)(\d{1,2})(\/)(\d{1,2})$/).required().label('delivery date')
    });

    const { error } = schema.validate(req.body);
    if (error) return res.status(422).json({ status: 0, message: error.details[0].message });
    next();

}

const updateDeliveryDateValidator = (req, res, next) => {
    const schema = Joi.object().keys({
        order_id: Joi.string().required().label('order id'),
        delivery_date: Joi.string().regex(/^(\d{4})(\/)(\d{1,2})(\/)(\d{1,2})$/).required().label('delivery date')
    });

    const { error } = schema.validate(req.body);
    if (error) return res.status(422).json({ status: 0, message: error.details[0].message });
    next();
}

const ordersListValidator = (req, res, next) => {
    const schema = Joi.object().keys({
        order_date: Joi.string().regex(/^(\d{4})(\/)(\d{1,2})(\/)(\d{1,2})$/).required().label('order date')
    });

    const { error } = schema.validate(req.body);
    if (error) return res.status(422).json({ status: 0, message: error.details[0].message });
    next();
}

const orderIdSearchValidator = (req, res, next) => {
    const schema = Joi.object().keys({
        order_id: Joi.string().required().label('order id'),
    });

    const { error } = schema.validate(req.body);
    if (error) return res.status(422).json({ status: 0, message: error.details[0].message });
    next();
}

module.exports = { createOrderValidate, updateDeliveryDateValidator, ordersListValidator, orderIdSearchValidator };