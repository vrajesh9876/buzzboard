const { Schema, model } = require('mongoose');

//  Creating the schema
const ordersSchema = new Schema({
    order_id: { type: String, unique: true, required: true },
    item_name: { type: String, required: true },
    cost: { type: String, required: true },
    order_date: { type: String, required: true },
    delivery_date: { type: String, required: true }
})

//  Creating the model
const Orders = model('orders', ordersSchema);

module.exports = { Orders };